# CarCar

Team:

* Person 1 - Which microservice?
Ryan Gibbs - Sales
* Person 2 - Which microservice?
Uzair Suleman - Service

## Design
https://excalidraw.com/#room=cf28ef9a6e8957371c51,4qivbhAZt01FV98M92WY0g

## Service microservice
The service microservice is in charge of managing all services for a car after its been sold. This allows for the creation, cancellation and fulfillment of a service appointment. If a car is brought in that was not sold from our inventory, they can still get service, but they wouldn't be considered VIP. VIP status is crossreferenced with the inventory api via the vin. If the vin was found to be a part of our inventory at any time the customer is given VIP status in terms of the services they wish to receive. The microservice also allows for the creation of  a technician, and viewing all service history appointments.

## Sales microservice
The sales microservice has the ultimate goal of managing sales. To that end, I needed to be able to create sales people, create potential customers, create sold cars that I could load the relevant information into, and get inventory data from the inventory microservice, which is loaded into an AutomobileVO model through polling the inventory MS. The sales person is simply a name and a unique employee number. The employee number can be used to get details about any sales that have that employee number associated with it. The potential customer has a name, and multiple fields for information related to location and contact info. They have an id associated with them, which could be used to find any autos that they have purchased, though this functionality was not setup. To create a sold car, you need a legitimate VIN number, which is checked for inside of the AutomobileVOs, the employee number of whoever made the sale, the customer ID for the customer, and a sale price. I decided to keep all automobiles within the database regardless of sale status. When a sale is made via the front end, a PUT request is sent to the inventory, indicating that the car was sold; this will then be updated in the AutomobileVO through polling. The front end handles all the display; if vehicle is sold, it will not display, although it is still within the inventory list.



## How to Run this Application
1. Launch the following applications on your computer. Docker desktop, and a terminal
2. Navigate to `https://gitlab.com/RGibbs96/project-beta`, and clone it inside of the directory of your choosing.
3. Navigate inside the project-beta directory and ensure that your current path contains the docker-compose.yml file, and the ghi, inventory, sales, and services directories.
4. cd into the project beta directory and run the following commands in sequential order
    1. `docker volume create beta-data`
    2. `docker-compose build`
    3. `docker-compose up`
        *if you're running macOS ,you might encounter a warning after the last comand, you can safely ignore this.
5. after the final command your docker containers will start running, however it may take a few minutes for the development server to start running and compile.

6. Wait until you see the notification in your terminal that the server has compiled, then navigate to: http://localhost:3000/

## Services
Frontend: localhost:3000 (Front End)
Inventory API: http://localhost:8100/
Sales API: http://localhost:8090/
Service API: http://localhost:8080/

## Api Sample Data

SERVICE REQUESTS

Create a Technician
POST Request to http://localhost:8080/api/technicians/
Request body:
{
    "technician_name": "John Doe",
    "employee_number": "123456"
}
Returns:
{
	"technician_name": "John Doe",
	"employee_number": "123456",
	"id": 1
}

List Technicians
GET Request to http://localhost:8080/api/technicians/
Returns:
{
	"technicians": [
		{
			"technician_name": "John Doe",
			"employee_number": "123456",
			"id": 1
		}
	]
}

Create an Appointment (NOTE THAT TECHNICIAN_NAME FIELD CORRESPONDS TO THE ID OF THE TECHNICIAN)
POST Request to http://localhost:8080/api/appointments/
Request body:
{
	"vin": "HONDA1",
	"customer_name": "Ryan",
	"date_time": "2023-01-27T17:34",
	"technician_name": "1",
	"reason": "Test Data"
}
Returns:
{
	"vin": "HONDA1",
	"customer_name": "Ryan",
	"date_time": "2023-01-27T17:34",
	"reason": "Test Data",
	"technician": {
		"technician_name": "John Doe",
		"employee_number": "123456",
		"id": 1
	},
	"is_completed": false,
	"vip": false,
	"id": 1,
	"cancelled": false
}

List Appointments
GET Request to http://localhost:8080/api/appointments/
Returns:
{
	"appointments": [
		{
			"vin": "HONDA1",
			"customer_name": "Ryan",
			"date_time": "2023-01-27T17:34:00+00:00",
			"reason": "Test Data",
			"technician": {
				"technician_name": "John Doe",
				"employee_number": "123456",
				"id": 1
			},
			"is_completed": false,
			"vip": false,
			"id": 1,
			"cancelled": false
		}
	]
}

INVENTORY REQUESTS

Create a manufacturer
POST Request to http://localhost:8100/api/manufacturers/
Request body:
{
	"name":"Honda"
}
Returns:
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Honda"
}

List all manufacturers
GET Request to http://localhost:8100/api/manufacturers/
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Honda"
		}
	]
}

Create a model
POST Request to http://localhost:8100/api/models/
Request body:
{
	"name":"Civic Type R",
	"picture_url":"https://bit.ly/3R3BwFp",
	"manufacturer_id":"1"
}
Returns:
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Civic Type R",
	"picture_url": "https://bit.ly/3R3BwFp",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Honda"
	}
}

List all models
GET Request to http://localhost:8100/api/models/
Returns:
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Civic Type R",
			"picture_url": "https://bit.ly/3R3BwFp",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Honda"
			}
		}
	]
}

Create an automobile
POST Request to http://localhost:8100/api/automobiles/
Request body:
{
	"color":"Red",
	"year":"2022",
	"vin":"HONDA2",
	"model_id":"1"
}
Returns:
{
	"href": "/api/automobiles/HONDA2/",
	"id": 1,
	"color": "Red",
	"year": "2022",
	"vin": "HONDA2",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Civic Type R",
		"picture_url": "https://bit.ly/3R3BwFp",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Honda"
		}
	},
	"is_sold": false
}

List all automobiles
GET Request to http://localhost:8100/api/automobiles/
Returns:
{
	"autos": [
		{
			"href": "/api/automobiles/HONDA2/",
			"id": 1,
			"color": "Red",
			"year": 2022,
			"vin": "HONDA2",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Civic Type R",
				"picture_url": "https://bit.ly/3R3BwFp",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Honda"
				}
			},
			"is_sold": false
		}
	]
}

SALES SERVICE

Create salesperson
POST Request to http://localhost:8090/api/sales/salesperson/
Request body:
{
	"name": "Ryan Gibbs",
	"employee_number": "100318"
}
Returns:
{
	"name": "Ryan Gibbs",
	"employee_number": "100318"
}

List of salespeople
GET Request to http://localhost:8090/api/sales/salesperson/
Returns:
{
	"sales_people": [
		{
			"name": "Ryan Gibbs",
			"employee_number": 100318
		}
	]
}

Detail view of sales person (NOTE THIS WILL NOT HAVE ANY AUTOS UNTIL A POST REQUEST TO MAKE A SOLD CAR ASSOCIATED WITH THIS SALES PERSON IS COMPLETED, SEE BELOW)
GET Request to http://localhost:8090/api/sales/salesperson/100318/
Returns:
{
	"autos": [],
	"sales_person": {
		"name": "Ryan Gibbs",
		"employee_number": 100318
	}
}

Create potential customer
POST Request to http://localhost:8090/api/sales/potentialcustomer/
Request body:
{
	"name": "Uzair",
	"address1": "1234 Funny Lane",
	"address2": "",
	"city": "San Jose",
	"zip_code": "95131",
	"phone_number": "1234567890"
}
Returns:
{
	"name": "Uzair",
	"address1": "1234 Funny Lane",
	"address2": "",
	"id": 1,
	"city": "San Jose",
	"state": "",
	"zip_code": "95131",
	"phone_number": "1234567890"
}

List potential customers
GET Request to http://localhost:8090/api/sales/potentialcustomer/
Returns:
{
	"potential_customers": [
		{
			"name": "Uzair",
			"address1": "1234 Funny Lane",
			"address2": "",
			"id": 1,
			"city": "San Jose",
			"state": "",
			"zip_code": "95131",
			"phone_number": "1234567890"
		}
	]
}

Create sold car (NOTE THAT THE LOGIC TO MARK A CAR AS SOLD IN INVENTORY IS HANDLED VIA REQUESTS MADE FROM THE FRONT END)
POST Request to http://localhost:8090/api/sales/soldautomobiles/
Request body:
{
	"vin": "HONDA2",
	"employee_number":"100318",
	"potential_customer_id": "1",
	"sell_price": "10000"
}
Returns:
{
	"href": "/api/sales/soldautomobiles/HONDA2/",
	"sell_price": "10000",
	"vin": "HONDA2",
	"automobile": {
		"is_sold": false,
		"id": 1,
		"import_href": "/api/automobiles/HONDA2/",
		"color": "Red",
		"year": 2022,
		"vin": "HONDA2",
		"model_name": "Civic Type R",
		"manufacturer_name": "Honda"
	},
	"sales_person": {
		"name": "Ryan Gibbs",
		"employee_number": 100318
	},
	"potential_customer": {
		"name": "Uzair",
		"address1": "1234 Funny Lane",
		"address2": "",
		"id": 1,
		"city": "San Jose",
		"state": "",
		"zip_code": "95131",
		"phone_number": "1234567890"
	}
}

List sold cars
GET Request to http://localhost:8090/api/sales/soldautomobiles/
Returns:
{
	"sold_automobiles": [
		{
			"href": "/api/sales/soldautomobiles/HONDA2/",
			"sell_price": 10000,
			"vin": "HONDA2",
			"automobile": {
				"is_sold": false,
				"id": 1,
				"import_href": "/api/automobiles/HONDA2/",
				"color": "Red",
				"year": 2022,
				"vin": "HONDA2",
				"model_name": "Civic Type R",
				"manufacturer_name": "Honda"
			},
			"sales_person": {
				"name": "Ryan Gibbs",
				"employee_number": 100318
			},
			"potential_customer": {
				"name": "Uzair",
				"address1": "1234 Funny Lane",
				"address2": "",
				"id": 1,
				"city": "San Jose",
				"state": "",
				"zip_code": "95131",
				"phone_number": "1234567890"
			}
		}
	]
}
