import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect , useState } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import TechniciansList from './TechniciansList';
import AppointmentsList from './AppointmentsList';
import AppointmentForm from './AppointmentForm';
import ServiceHistory from './ServiceHistory';
import InventoryList from './common/InventoryList'
import NewSalesPersonForm from './common/NewSalesPersonForm';
import SoldAutomobileList from './common/SoldAutomobileList';
import SalesPersonHistoryList from './common/SalesPersonHistoryList';
import NewPotentialCustomerForm from './common/NewPotentialCustomerForm';
import NewSaleRecordForm from './common/CreateSaleRecordForm';
import NewAutoToInventory from './common/AddAutoToInventory';
import NewManufacturerForm from './common/NewManufacturerForm';
import ManufacturersList from './common/ManufacturersList';
import NewModelForm from './common/CreateModelForm';
import ModelsList from './common/ModelList';



function App() {
  const [technicians, setTechnicians] = useState([])
  const [appointments, setAppointments] = useState([])
  const [autos, setAutos] = useState([])
  const [salesPeople, setSalesPeople] = useState([])
  const [soldAutos, setSoldAutos] = useState([])
  const [potentialCustomers, setPotentialCustomers] = useState([])
  const [models, setModels] = useState([])
  const [manufacturers, setManufacturers] = useState([])







  const getTechnicians = async () => {
    const url = 'http://localhost:8080/api/technicians/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      const technicians = data.technicians
      setTechnicians(technicians)
    }
  }




  const fetchAutos = async () => {
    const url = "http://localhost:8100/api/automobiles/"
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setAutos(data.autos)
    }
  }



  const getAppointments = async () => {
    const url = 'http://localhost:8080/api/appointments/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      const appointments = data.appointments
      setAppointments(appointments)
    }
  }

  const fetchSalesPeople = async () => {
    const url = "http://localhost:8090/api/sales/salesperson/"
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setSalesPeople(data.sales_people)
    }
  }


  const fetchSoldAutos = async () => {
    const url = "http://localhost:8090/api/sales/soldautomobiles/"
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setSoldAutos(data.sold_automobiles)
    }
  }

  const fetchPotentialCustomers = async () => {
    const url = "http://localhost:8090/api/sales/potentialcustomer/"
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setPotentialCustomers(data.potential_customers)
    }
  }

  const fetchModels = async () => {
    const url = "http://localhost:8100/api/models/"
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setModels(data.models)
    }
  }



  const fetchManufacturers = async () => {
    const url = "http://localhost:8100/api/manufacturers/"
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setManufacturers(data.manufacturers)
    }
  }


  useEffect(() => {
    fetchAutos()
    fetchSalesPeople()
    fetchSoldAutos()
    fetchPotentialCustomers()
    fetchModels()
    fetchManufacturers()
    getTechnicians()
    getAppointments()
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">

        <Routes>
          <Route path="/" element={<MainPage />} />




          <Route path="appointments">
            <Route path="" element={<AppointmentsList appointments={appointments} getAppointments={getAppointments} />}/>
            <Route path="new" element={<AppointmentForm getAppointments={getAppointments} technicians={technicians} />}/>
            <Route path="history"  element={<ServiceHistory appointments={appointments} setAppointments={setAppointments}/>}/>
          </Route>

          <Route path="technicians">
            <Route path="" element={<TechniciansList technicians={technicians} getTechnicians={getTechnicians} />}/>
            <Route path="new" element={<TechnicianForm getTechnicians={getTechnicians}/>} />
          </Route>
          <Route path="inventory">
              <Route path="automobiles/all" element={<InventoryList autos={autos} fetchAutos={fetchAutos}/>} />
              <Route path="automobiles/new" element={<NewAutoToInventory models={models} fetchAutos={fetchAutos} />} />
              <Route path="manufacturers/all" element={<ManufacturersList manufacturers={manufacturers}/>} />
              <Route path="manufacturers/new" element={<NewManufacturerForm fetchManufacturers={fetchManufacturers}/>} />
              <Route path="models/all" element={<ModelsList models={models}/>} />
              <Route path="models/new" element={<NewModelForm manufacturers={manufacturers} fetchModels={fetchModels}/>} />
          </Route>
          <Route path="potentialcustomers">
              <Route path="new" element={<NewPotentialCustomerForm fetchPotentialCustomers={fetchPotentialCustomers} />} />
          </Route>
          <Route path="salespeople">
              <Route path="new" element={<NewSalesPersonForm fetchSalesPeople={fetchSalesPeople} />} />
          </Route>
          <Route path="sales">
              <Route path="all" element={<SoldAutomobileList soldAutos={soldAutos} salesPeople={salesPeople} />} />
              <Route path="new" element={<NewSaleRecordForm salesPeople={salesPeople} autos={autos} potentialCustomers={potentialCustomers} fetchSoldAutos={fetchSoldAutos} />} />
              <Route path="history" element={<SalesPersonHistoryList salesPeople={salesPeople} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;
