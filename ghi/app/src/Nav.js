import { NavLink } from 'react-router-dom'
import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import NavDropdown from 'react-bootstrap/NavDropdown'
import Dropdown from 'react-bootstrap/Dropdown'

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li>
              <Dropdown>
                <Dropdown.Toggle variant="success" id="dropdown-basic">
                  Service
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <NavLink className="nav-link" to="/technicians/" style={{color:'black'}}>Technician Team</NavLink>
                  <NavLink className="nav-link" to="/technicians/new" style={{color:'black'}}>Create a Technician</NavLink>
                  <NavLink className="nav-link" to="/appointments/" style={{color:'black'}}>Service Appointments</NavLink>
                  <NavLink className="nav-link" to="/appointments/new" style={{color:'black'}}>Create Appointment</NavLink>
                  <NavLink className="nav-link" to="/appointments/history/" style={{color:'black'}}>Service History</NavLink>
                </Dropdown.Menu>
              </Dropdown>
            </li>
            <li>
              <Dropdown>
                <Dropdown.Toggle variant="success" id="dropdown-basic">
                  Sales
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <NavLink className="nav-link" to="/salespeople/new/" style={{color:'black'}}>Create sales person</NavLink>
                  <NavLink className="nav-link" to="/potentialcustomers/new/" style={{color:'black'}}>Create customer</NavLink>
                  <NavLink className="nav-link" to="/sales/new/" style={{color:'black'}}>Create sales record</NavLink>
                  <NavLink className="nav-link" to="/sales/all/" style={{color:'black'}}>All sales</NavLink>
                  <NavLink className="nav-link" to="/sales/history/" style={{color:'black'}}>Sales by employee</NavLink>
                </Dropdown.Menu>
              </Dropdown>
            </li>
            <li>
              <Dropdown>
                <Dropdown.Toggle variant="success" id="dropdown-basic">
                  Inventory
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <NavLink className="nav-link" to="/inventory/automobiles/all/" style={{color:'black'}}>Current inventory</NavLink>
                  <NavLink className="nav-link" to="/inventory/automobiles/new/" style={{color:'black'}}>Add vehicle to inventory</NavLink>
                  <NavLink className="nav-link" to="/inventory/manufacturers/new/" style={{color:'black'}}>Add manufacturer</NavLink>
                  <NavLink className="nav-link" to="/inventory/manufacturers/all/" style={{color:'black'}}>All manufacturers</NavLink>
                  <NavLink className="nav-link" to="/inventory/models/new/" style={{color:'black'}}>Add model</NavLink>
                  <NavLink className="nav-link" to="/inventory/models/all/" style={{color:'black'}}>All models</NavLink>
                </Dropdown.Menu>
              </Dropdown>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
