import React from 'react'
import { NavLink} from "react-router-dom"

function TechnicianList(props) {
  const handleDeleteClick = async (event) => {
    const id = event.target.value
    const response = await fetch (`http://localhost:8080/api/Technician/${id}/`,{
      method: "delete",
    })
    if (response.ok){
        props.getTechnician()
    }
  }

  if (props.technicians === undefined) {
    return null
  }


  return (
    <div>
      <NavLink className="nav-link" to="/technicians/new">Add a new Technician</NavLink>

      <table className="table table-striped table-hover align-middle mt-5">
        <thead>
          <tr>
            <th>Technician Name</th>
            <th>Employee Number</th>
            {/* <th>Delete Technician</th> */}
          </tr>
        </thead>
        <tbody>
          {props.technicians.map(technician => {
            return (
              <tr key={technician.id}>
                <td>{ technician.technician_name }</td>
                <td>{ technician.employee_number }</td>
                {/* <td><button type="button" value={technician.id} onClick={() => handleDeleteClick(technician.id)}>Delete</button></td> */}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )
}

export default TechnicianList;
