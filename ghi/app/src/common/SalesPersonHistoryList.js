import { NavLink } from 'react-router-dom'
import React, {useEffect, useState } from "react"


function SalesPersonHistoryList(props){

    const [salesPerson, setSalesPerson] = useState("")
    const [salesPersonDetails, setSalesPersonDetails] = useState([])

    const handleSalesPersonChange = async (event) => {
        const value = event.target.value
        setSalesPerson(value)
        if (value != ""){
            const url = `http://localhost:8090/api/sales/salesperson/${value}/`
            const response = await fetch(url)
            if (response.ok) {
                const data = await response.json()
                setSalesPersonDetails(data.autos)
            }
    } else {
        setSalesPersonDetails([])
    }
    }

    return (
        <div>
            <h1>Sales person history</h1>
            <div className="mb-3">
                <select value={salesPerson} onChange={handleSalesPersonChange} required name="sales_person" id="sales_person" className="form-select">
                    <option value="">Select sales person</option>
                    {props.salesPeople.map(salesPerson => {
                        return (
                            <option value={salesPerson.employee_number} key={salesPerson.employee_number}>
                                {salesPerson.name}, #{salesPerson.employee_number}
                            </option>
                         );
                    })}
                </select>
            </div>
                <table className="table table-striped table-hover align-middle mt-5">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Purchaser Name</th>
                            <th>Automobile VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {salesPersonDetails.map(detail => {
                            if(salesPersonDetails.length>0)
                                return (<tr key={detail.href}>
                                            <td>{detail.sales_person.name}</td>
                                            <td>{detail.potential_customer.name}</td>
                                            <td>{detail.vin}</td>
                                            <td>${detail.sell_price}</td>
                                        </tr>
                                )
                        })}

                    </tbody>
                </table>
            </div>

    )

}

export default SalesPersonHistoryList
