import React from 'react'

function ManufacturersList(props) {
    return (
        <div>
            <table className="table table-striped table-hover align-middle mt-5">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {props.manufacturers.map(manufacturer => {
                        return(
                            <tr key={manufacturer.href}>
                                <td>{manufacturer.name}</td>
                            </tr>
                    )
                    })}
                </tbody>
            </table>
        </div>
    )
}


export default ManufacturersList
