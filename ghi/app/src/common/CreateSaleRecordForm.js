import React, {useEffect, useState } from "react";

function NewSaleRecordForm(props) {

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.vin = auto
        data.employee_number = salesPerson
        data.potential_customer_id = potentialCustomer
        data.sell_price = sellPrice
        const sellAutoUrl = "http://localhost:8090/api/sales/soldautomobiles/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(sellAutoUrl, fetchConfig)
        if (response.ok) {
            const isSoldUpdate = {"is_sold": true}
            const isSoldUpdateUrl = `http://localhost:8100/api/automobiles/${auto}/`
            const config = {
                method: "put",
                body: JSON.stringify(isSoldUpdate),
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            const updateResponse = await fetch(isSoldUpdateUrl,config)
            if (updateResponse.ok){
                setAuto("")
                setSalesPerson("")
                setPotentialCustomer("")
                setSellPrice("")
                props.fetchSoldAutos()
            }
        }
    }
    function checkIfSold(auto) {
        return auto.is_sold === false
    }
    const unsold_autos = props.autos.filter(checkIfSold)

    const [auto, setAuto] = useState("")
    const [salesPerson, setSalesPerson] = useState("")
    const [potentialCustomer, setPotentialCustomer] = useState("")
    const [sellPrice, setSellPrice] = useState("")

    const handleAutoChange = (event) => {
        const value = event.target.value
        setAuto(value)
    }
    const handleSalesPersonChange = (event) => {
        const value = event.target.value
        setSalesPerson(value)
    }
    const handlePotentialCustomerChange = (event) => {
        const value = event.target.value
        setPotentialCustomer(value)
    }
    const handleSellPriceChange = (event) => {
        const value = event.target.value
        setSellPrice(value)
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new sales record</h1>
                    <form onSubmit={handleSubmit} id="create-new-sales-person-form">
                        <div className="mb-3">
                            <select value={auto} onChange={handleAutoChange} required name="auto" id="auto" className="form-select">
                                <option value="">Select a car</option>
                                {unsold_autos.map(auto => {
                                    return (
                                        <option value={auto.vin} key={auto.vin}>
                                            {auto.year} {auto.model.manufacturer.name} {auto.model.name}, {auto.color}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={salesPerson} onChange={handleSalesPersonChange} required name="sales_person" id="sales_person" className="form-select">
                                <option value="">Select a sales person</option>
                                {props.salesPeople.map(salesPerson => {
                                    return (
                                        <option value={salesPerson.employee_number} key={salesPerson.employee_number}>
                                            {salesPerson.name}, #{salesPerson.employee_number}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={potentialCustomer} onChange={handlePotentialCustomerChange} required name="potential_customer" id="potential_customer" className="form-select">
                                <option value="">Select a customer</option>
                                {props.potentialCustomers.map(potentialCustomer=> {
                                    return (
                                        <option value={potentialCustomer.id} key={potentialCustomer.id}>
                                            {potentialCustomer.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={sellPrice} onChange={handleSellPriceChange} required type="number" name="sell_price" id="sell_price" className="form-control"/>
                            <label htmlFor="sell_price">Sale price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default NewSaleRecordForm
