import React from 'react'

function ModelsList(props) {
    return (
        <div>
            <table className="table table-striped table-hover align-middle mt-5">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {props.models.map(model => {
                        return(
                            <tr key={model.href}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td><img src={model.picture_url} alt={model.href} width={350} height={250}/></td>
                            </tr>
                    )
                    })}
                </tbody>
            </table>
        </div>
    )
}


export default ModelsList
