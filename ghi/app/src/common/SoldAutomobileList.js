import { NavLink } from 'react-router-dom'
import React, {useEffect, useState } from "react"


function SoldAutomobileList(props){

    const [salesPerson, setSalesPerson] = useState("")

    const handleSalesPersonChange = (event) => {
        const value = event.target.value
        setSalesPerson(value)
        console.log(value)
    }

    return (
        <div>
            <h1>Sold Automobiles</h1>
            <table className="table table-striped table-hover align-middle mt-5">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Employee Number</th>
                        <th>Purchaser Name</th>
                        <th>Automobile VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {props.soldAutos.map(soldAuto => {
                        return (
                            <tr key={soldAuto.href}>
                                <td>{soldAuto.sales_person.name}</td>
                                <td>{soldAuto.sales_person.employee_number}</td>
                                <td>{soldAuto.potential_customer.name}</td>
                                <td>{soldAuto.vin}</td>
                                <td>${soldAuto.sell_price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )

}

export default SoldAutomobileList
