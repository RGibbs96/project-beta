import React, {useEffect, useState } from "react";

function NewPotentialCustomerForm(props) {

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name
        data.address1 = address1
        data.address2 = address2
        data.city = city
        data.state = state
        data.zip_code = zipCode
        data.phone_number = phoneNumber
        const customerUrl = "http://localhost:8090/api/sales/potentialcustomer/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(customerUrl, fetchConfig)
        if (response.status === 400) {
            console.log("error handling here")
        }
        if (response.ok) {
            setName("")
            setAddress1("")
            setAddress2("")
            setCity("")
            setState("")
            setZipCode("")
            setPhoneNumber("")
            props.fetchPotentialCustomers()
        }
}

    const [name, setName] = useState("")
    const [address1, setAddress1] = useState("")
    const [address2, setAddress2] = useState("")
    const [city, setCity] = useState("")
    const [state, setState] = useState("")
    const [zipCode, setZipCode] = useState("")
    const [phoneNumber, setPhoneNumber] = useState("")

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    const handleAddress1Change = (event) => {
        const value = event.target.value
        setAddress1(value)
    }
    const handleAddress2Change = (event) => {
        const value = event.target.value
        setAddress2(value)
    }
    const handleCityChange = (event) => {
        const value = event.target.value
        setCity(value)
    }
    const handleStateChange = (event) => {
        const value = event.target.value
        setState(value)
    }
    const handleZipCodeChange = (event) => {
        const value = event.target.value
        setZipCode(value)
    }
    const handlePhoneNumberChange = (event) => {
        const value = event.target.value
        setPhoneNumber(value)
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new potential customer</h1>
                    <form onSubmit={handleSubmit} id="create-new-sales-person-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleNameChange} required type="text" name="name" id="name" className="form-control" />
                            <label>Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={address1} onChange={handleAddress1Change} required type="text" name="address1" id="address1" className="form-control" />
                            <label>Address 1</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={address2} onChange={handleAddress2Change} type="text" name="address2" id="address2" className="form-control" />
                            <label>Address 2 (optional)</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={city} onChange={handleCityChange} required type="text" name="city" id="city" className="form-control" />
                            <label>City</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={state} onChange={handleStateChange} required type="text" name="state" id="state" className="form-control" />
                            <label>State</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={zipCode} onChange={handleZipCodeChange} required type="text" name="zipCode" id="zipCode" className="form-control" />
                            <label>Zip Code, Max 5 char limit</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={phoneNumber} onChange={handlePhoneNumberChange} required type="text" name="phoneNumber" id="phoneNumber" className="form-control" />
                            <label>Phone Number, Ex:1234567890</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default NewPotentialCustomerForm
