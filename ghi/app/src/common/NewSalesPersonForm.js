import React, {useEffect, useState } from "react";

function NewSalesPersonForm(props) {

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name
        data.employee_number = employeeNumber
        const salePersonUrl = "http://localhost:8090/api/sales/salesperson/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(salePersonUrl, fetchConfig)
        if (response.status === 400) {
            console.log("error handling here")
        }
        if (response.ok) {
            setName("")
            setEmployeeNumber("")
            props.fetchSalesPeople()
        }
}

    const [name, setName] = useState("")
    const [employeeNumber, setEmployeeNumber] = useState("")

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value
        setEmployeeNumber(value)
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new sales person</h1>
                    <form onSubmit={handleSubmit} id="create-new-sales-person-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleNameChange} required type="text" name="name" id="name" className="form-control" />
                            <label>Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={employeeNumber} onChange={handleEmployeeNumberChange} required type="number" name="employee_number" id="employee_number" className="form-control" />
                            <label>Employee Number (Unique)</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default NewSalesPersonForm
