import React, {useState} from 'react'

function NewManufacturerForm(props) {

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = manufacturerName
        const url = "http://localhost:8100/api/manufacturers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setManufacturerName("")
            props.fetchManufacturers()
        }
    }

    const [manufacturerName, setManufacturerName] = useState('')

    const handleManufacturerNameChange = (event) => {
        const value = event.target.value
        setManufacturerName(value)
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-new-sales-person-form">
                        <div className="form-floating mb-3">
                            <input value={manufacturerName} onChange={handleManufacturerNameChange} required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label>Manufacturer name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default NewManufacturerForm
