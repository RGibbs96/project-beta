# Generated by Django 4.0.3 on 2023-01-25 07:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0006_rename_auto_mobile_soldautomobile_automobile_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='automobilevo',
            name='is_sold',
            field=models.BooleanField(default=False),
        ),
    ]
