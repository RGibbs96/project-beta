from common.json import ModelEncoder

from .models import AutomobileVO, SalesPerson, PotentialCustomer, SoldAutomobile

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "is_sold",
        "id",
        "import_href",
        "color",
        "year",
        "vin",
        "model_name",
        "manufacturer_name",
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
    ]

class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = [
        "name",
        "address1",
        "address2",
        "id",
        "city",
        "state",
        "zip_code",
        "phone_number",
    ]

class SoldAutomobileEncoder(ModelEncoder):
    model = SoldAutomobile
    properties = [
        "sell_price",
        "vin",
        "automobile",
        "sales_person",
        "potential_customer",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "potential_customer": PotentialCustomerEncoder(),
    }
