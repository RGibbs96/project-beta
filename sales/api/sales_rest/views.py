from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, SalesPerson, PotentialCustomer, SoldAutomobile
from .encoders import AutomobileVOEncoder, SalesPersonEncoder, PotentialCustomerEncoder, SoldAutomobileEncoder

def api_AutomobileVO(request):
    """
    List view to confirm that AutomobileVOs are polling correctly.
    """
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse(
            {"autos":autos},
            encoder=AutomobileVOEncoder
        )
@require_http_methods(["GET","POST"])
def api_sales_person(request):
    """
    List view of all sales people, as well as the functionality to create a new sales person.
    """
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people":sales_people},
            encoder=SalesPersonEncoder
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            print(content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )

        except:
            response = JsonResponse(
                {"message": "Could not create the sales person"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET"])
def api_sales_person_detail(request, employee_number):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(employee_number=employee_number)
            autos = SoldAutomobile.objects.filter(sales_person=sales_person)
            autosResponse = JsonResponse(
                {"autos":autos,"sales_person":{"name":sales_person.name,"employee_number":sales_person.employee_number}},
                SoldAutomobileEncoder,
                safe=False
            )
            return autosResponse


        except :
            return JsonResponse(
                {"message": "one of the gets failed"}
            )

@require_http_methods(["GET","POST"])
def api_potential_customer(request):
    """
    List view of all potential customers, as well as functionality to create a new potential customer.
    """
    if request.method == "GET":
        potential_customers = PotentialCustomer.objects.all()
        return JsonResponse(
            {"potential_customers": potential_customers},
            encoder=PotentialCustomerEncoder,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            potential_customer = PotentialCustomer.objects.create(**content)
            return JsonResponse(
                potential_customer,
                encoder=PotentialCustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the potential customer"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET","POST"])
def api_sold_automobile(request):
    """
    List all sold automobiles and add a new automobile to the sold automobiles. To sell an automobile, it must be an AutomobileVO object, and is_sold must == False.
    When making call from front-end, will have a put request to update inventory, and allow this to update the AutomobileVO
    """
    if request.method == "GET":
        sold_automobiles = SoldAutomobile.objects.all()
        return JsonResponse(
            {"sold_automobiles": sold_automobiles},
            encoder=SoldAutomobileEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        new_content = {}
        try:
            input_vin = content["vin"]
            employee_number_input = content["employee_number"]
            potential_customer_id = content["potential_customer_id"]
            automobile_vo_from_vin = AutomobileVO.objects.get(vin=input_vin)
            sales_person = SalesPerson.objects.get(employee_number=employee_number_input)
            potential_customer = PotentialCustomer.objects.get(id=potential_customer_id)
            new_content["vin"] = input_vin
            new_content["sell_price"] = content["sell_price"]
            new_content["automobile"] = automobile_vo_from_vin
            new_content["sales_person"] = sales_person
            new_content["potential_customer"] = potential_customer
        except (AutomobileVO.DoesNotExist, SalesPerson.DoesNotExist, PotentialCustomer.DoesNotExist) as e:
            return JsonResponse(
                {"message":"Something here does not exist"}
            )
        sold_automobile = SoldAutomobile.objects.create(**new_content)

        return JsonResponse(
            sold_automobile,
            encoder=SoldAutomobileEncoder,
            safe=False
        )
