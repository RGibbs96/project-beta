from django.urls import path
from .api_views import api_technicians, api_technician, api_appointments, api_appointment, api_appointments_by_vin, api_automobile_VO_list


urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("technicians/<int:id>/", api_technician, name="api_technician"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/<int:id>/", api_appointment, name="api_appointment"),
    path("appointments/<str:vin>/", api_appointments_by_vin, name="api_appointments_by_vin"),
    path("automobileVOs/", api_automobile_VO_list, name="api_automobile_VO_list"),
]
